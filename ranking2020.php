<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" href="style.css">
</head>
<body>

<?php

// define types

class Score {
	public $person;
	public $match;
	public $result;
	public $nextScore;
	public $prevScore;
	public $nextPersonScore;
	public $prevPersonScore;
	public $rankingBefore;
	public $rankingAfter;
}

class Person {
	public $firstName;
	public $lastName;
	public $division;
	public $firstScore;
	public $finalRanking;
}

class Match {
	public $matchCode;
	public $matchName;
	public $matchDate;
	public $weight = null;
	public $firstScore;
	public $prevMatch;
	public $nextMatch;
	
	public function calculateWeight() {
		$this->weight = 0.5;
		
		$cScore = $this->firstScore;
		while($cScore <> null) {
			if($cScore->result > 0) {
				if($cScore->rankingBefore->personRank == 1) {
					$this->weight = max($this->weight, 1);
				} else if($cScore->rankingBefore->personRank == 2) {
					$this->weight = max($this->weight, 0.8);
				} else if($cScore->rankingBefore->personRank == 3) {
					$this->weight = max($this->weight, 0.6);
				}
			}
			
			$cScore = $cScore->nextPersonScore;
		}
	}
}

class Ranking {
	public $points = null;
	public $personRank = 0;
	public $weight = 0;
	public $scoreBefore;
	public $scoreAfter;
	
	public function percentPoints() {
        if($this->points <> null) {
			$cScore = $this->scoreBefore->match->firstScore;
			$maxPoints = $cScore->rankingAfter->points;
			while($cScore <> null) {
				if($cScore->rankingAfter->points <> null && $cScore->rankingAfter->points > $maxPoints) {
					$maxPoints = $cScore->rankingAfter->points;
				}

				$cScore = $cScore->nextPersonScore;
			}
			
			return round(100 / $maxPoints * $this->points, 2, PHP_ROUND_HALF_UP);
		} else {
			return null;
		}
    }
}

// get data from database and create objects structure

$conn = new mysqli("localhost", "results", "passwd", "rdpldata");
mysqli_set_charset($conn,"utf8");

if ($conn->connect_error) {
  die("connection failed: " . $conn->connect_error);
}

$sql = "select lastName, firstName, shooterNumber, matchCode, matchName, matchDate, matchDivision, matchResult from scores
where matchDivision = 'OPEN'
order by matchDivision, lastName, firstName, matchDate";

$result = $conn->query($sql);

$score = new Score;
$score->prevScore = null;
$score->prevPersonScore = null;

$firstScore = $score;
$prevPersonScore = $score;

$shooterNumber = 0;
$matchDivision = 'NONE';
while($row = $result->fetch_assoc()) {
	if($shooterNumber <> $row["shooterNumber"]) {
		if($shooterNumber <> 0) {
			$score->prevScore->nextScore = null;
			$score->prevScore = null;
			$prevPersonScore->nextPersonScore = $score;
			$score->prevPersonScore = $prevPersonScore;
			$prevPersonScore = $score;

			if($matchDivision <> $row["matchDivision"] && $matchDivision <> 'NONE') {
				// division change
			}
		}
		  
		$score->person = new Person;
		$score->person->firstScore = $score;
		$score->person->firstName = $row["firstName"];
		$score->person->lastName = $row["lastName"];
		$score->person->division = $row["matchDivision"];
	} else {
		$score->prevPersonScore = $score->prevScore->prevPersonScore <> null ? $score->prevScore->prevPersonScore->nextScore : null;
		
		if($score->prevPersonScore <> null) {
			$score->prevPersonScore->nextPersonScore = $score;
		}
		
		$score->person = $score->prevScore->person;
	}
	
	// if we are processing first row of data then we create match objects and connect them together
	if($prevPersonScore === $firstScore) {
		$score->match = new Match;
		$score->match->matchCode = $row["matchCode"];
		$score->match->matchName = $row["matchName"];
		$score->match->matchDate = $row["matchDate"];
		$score->match->firstScore = $score;
		
		if($score->prevScore <> null) {
			$score->match->prevMatch = $score->prevScore->match;
			$score->prevScore->match->nextMatch = $score->match;
		}
	} else { // otherwise we just assign existing match object to the score
		$score->match = $score->prevPersonScore->match;
	}
	
	$score->result = round($row["matchResult"], 2, PHP_ROUND_HALF_UP);
	$score->nextScore = new Score;
	$score->nextScore->prevScore = $score;

	// create empty ranking objects and connect them together
	if($score->prevScore <> null) {
		$score->rankingBefore = $score->prevScore->rankingAfter;
		$score->rankingBefore->scoreAfter = $score;
	} else {
		$score->rankingBefore = new Ranking;
		$score->rankingBefore->scoreBefore = null;
		$score->rankingBefore->scoreAfter = $score;
	}
	
	$score->rankingAfter = new Ranking;
	$score->rankingAfter->scoreBefore = $score;
	$score->rankingAfter->scoreAfter = null;

	$score = $score->nextScore;
	
	$shooterNumber = $row["shooterNumber"];
	$matchDivision = $row["matchDivision"];
}

$score->prevScore->nextScore = null;

$conn->close();

// calculate ranking

$PREMIUM = 1;
$MINIMUM_WEIGHT = 3;
$LIDER_1 = 96;
$LIDER_2 = 92;
$LIDER_3 = 86;

// caculate first ranking after first 2 matches
$firstScore->match->weight = 1;
$firstScore->nextScore->match->weight = 1;

$currentScore = $firstScore;
while($currentScore <> null) {
	if($currentScore->result > 0 || $currentScore->nextScore->result > 0) {
		if($currentScore->result > 0 && $currentScore->nextScore->result > 0) {
			$currentScore->nextScore->rankingAfter->weight = 2;
			$currentScore->nextScore->rankingAfter->points = round(($currentScore->result + $currentScore->nextScore->result) / 2 + $PREMIUM * 2, 2, PHP_ROUND_HALF_UP);
		} else {
			$currentScore->nextScore->rankingAfter->weight = 1;
			$currentScore->nextScore->rankingAfter->points = max($currentScore->result, $currentScore->nextScore->result) + $PREMIUM;
		}
	}
	
	$currentScore = $currentScore->nextPersonScore;
}

// we give lider points based on ranking points
$currentScore = $firstScore;
while($currentScore <> null) {
	if($currentScore->nextScore->rankingAfter->points <> null) {
		if($currentScore->nextScore->rankingAfter->points >= $LIDER_1) {
			$currentScore->nextScore->rankingAfter->personRank = 1;
		} else if ($currentScore->nextScore->rankingAfter->points >= $LIDER_2) {
			$currentScore->nextScore->rankingAfter->personRank = 2;
		} else if ($currentScore->nextScore->rankingAfter->points >= $LIDER_3) {
			$currentScore->nextScore->rankingAfter->personRank = 3;
		}
	}
	
	$currentScore = $currentScore->nextPersonScore;
}

// calculate next rankings
$currentScore = $firstScore->nextScore->nextScore;
$prevCurrentScore = $currentScore;
while($currentScore <> null) {
	if($currentScore->match->weight == null) {
		$currentScore->match->calculateWeight();
	}
	
	if($currentScore->result > 0) {
		if($currentScore->rankingBefore->points <> null) {
			$currentScore->rankingAfter->points = round(
				($currentScore->rankingBefore->points * $currentScore->rankingBefore->weight + $currentScore->result * $currentScore->match->weight) /
				($currentScore->rankingBefore->weight + $currentScore->match->weight) + $PREMIUM
			, 2, PHP_ROUND_HALF_UP);

			$currentScore->rankingAfter->weight = $currentScore->rankingBefore->weight + $currentScore->match->weight;
		} else {
			$currentScore->rankingAfter->points = $currentScore->result + $PREMIUM;
			$currentScore->rankingAfter->weight = $currentScore->match->weight;
		}
	} else {
		$currentScore->rankingAfter->points = $currentScore->rankingBefore->points;
		$currentScore->rankingAfter->weight = $currentScore->rankingBefore->weight;
	}
	
	// we give lider points based on ranking points
	if($currentScore->rankingAfter->points <> null && $currentScore->match->weight == 1) {
		if($currentScore->rankingAfter->points >= $LIDER_1) {
			$currentScore->rankingAfter->personRank = 1;
		} else if ($currentScore->rankingAfter->points >= $LIDER_2) {
			$currentScore->rankingAfter->personRank = 2;
		} else if ($currentScore->rankingAfter->points >= $LIDER_3) {
			$currentScore->rankingAfter->personRank = 3;
		}
	} else {
		$currentScore->rankingAfter->personRank = $currentScore->rankingBefore->personRank;
	}

	if($currentScore->nextPersonScore <> null) {
		$currentScore = $currentScore->nextPersonScore;
	} else {
		$currentScore = $prevCurrentScore->nextScore;
		$prevCurrentScore = $currentScore;
	}
}

// assign final ranking
$lastScore = $firstScore;
while($lastScore->nextScore <> null) {
	$lastScore = $lastScore->nextScore;
}

while($lastScore <> null) {
	if($lastScore->rankingAfter->points <> null) {
		if($lastScore->rankingAfter->weight < $MINIMUM_WEIGHT) {
			$lastScore->person->finalRanking = 
				round($lastScore->rankingAfter->percentPoints() * $lastScore->rankingAfter->weight / $MINIMUM_WEIGHT, 2, PHP_ROUND_HALF_UP);
		} else {
			$lastScore->person->finalRanking = $lastScore->rankingAfter->percentPoints();
		}
	} else {
		$lastScore->person->finalRanking = 0;
	}

	$lastScore = $lastScore->nextPersonScore;
}

// print data on a page

echo "\n<table>\n<tr>\n<th class='headerName'>Zawodnik</th>";

$currentScore = $firstScore;
while($currentScore <> null) {
	echo "<th class='headerMatch'>". $currentScore->match->matchCode. "/". $currentScore->match->weight. "</th>";
	if($currentScore <> $firstScore) {
		echo "<th class='headerRanking'>Ranking</th>";
	}

	$currentScore = $currentScore->nextScore;
}

echo "<th class='headerRanking'>Finalnie</th></tr>\n<tr>\n";

$currentScore = $firstScore;
$prevCurrentScore = $currentScore;
while($currentScore <> null) {
	if($currentScore === $prevCurrentScore) {
		echo "<td class='name'>". $currentScore->person->lastName. " ". $currentScore->person->firstName. "</td>";
	}
	
	echo "<td class='result'>". $currentScore->result. "</td>";
	if($currentScore <> $prevCurrentScore) {
		echo "<td class='ranking'>". $currentScore->rankingAfter->points;
		if($currentScore->rankingAfter->weight > 0) {
			echo "/". $currentScore->rankingAfter->weight;
		}
		if($currentScore->rankingAfter->personRank > 0) {
			echo "*". $currentScore->rankingAfter->personRank;
		}
		echo "</td>";
	}
	
	if($currentScore->nextScore <> null) {
		$currentScore = $currentScore->nextScore;
	} else {
		echo "<td class='fianlRanking'>". $currentScore->person->finalRanking. "</td>";

		if($prevCurrentScore->nextPersonScore <> null) {
			echo "</tr>\n<tr>\n";
		}

		$currentScore = $prevCurrentScore->nextPersonScore;
		$prevCurrentScore = $currentScore;
	}
}

echo "</tr>\n</table>\n";

?>

  
<script type="text/javascript">
	const getCellValue = (tr, idx) => {
		$value = tr.children[idx].innerText || tr.children[idx].textContent;
		
		if($value == '') {
			return 0;
		} else {
			if($value.includes('/')) {
				return $value.substr(0, $value.indexOf('/'));;
			} else {
				return $value;
			}
		}
	}

	const comparer = (idx, desc) => (a, b) => ((v1, v2) => 
		v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
		)(getCellValue(desc ? a : b, idx), getCellValue(desc ? b : a, idx));

	document.querySelectorAll('th').forEach(th => th.addEventListener('click', (() => {
		const table = th.closest('table');
		Array.from(table.querySelectorAll('tr:nth-child(n+2)'))
			.sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.desc = !this.desc))
			.forEach(tr => table.appendChild(tr) );
	})));
</script>  

</body>
</html>